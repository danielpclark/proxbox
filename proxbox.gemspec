$:.push File.expand_path("../lib", __FILE__)
require "proxbox/version"
 
Gem::Specification.new do |s|
  s.name        = 'proxbox'
  s.version     = ProxBox::VERSION
  s.licenses    = ['Attribution-NonCommercial-NoDerivatives 4.0 International']
  s.summary     = "Proximity Box Detector"
  s.description = "Proximity Box Detector.  Relationship by closeness."
  s.authors     = ["Daniel P. Clark / 6ftDan(TM)"]
  s.email       = 'webmaster@6ftdan.com'
  s.files       = ['lib/proxbox/version.rb', 'lib/proxbox.rb', 'LICENSE']
  s.homepage    = 'https://bitbucket.org/danielpclark/proxbox'
  s.platform    = 'ruby'
  s.require_paths = ['lib']
  s.required_ruby_version = '>= 1.9.1'
end