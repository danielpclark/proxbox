# Title::     Proximity Box (proxbox)
# Author::    Daniel P. Clark  (mailto:6ftdan@gmail.com)
# License::   Attribution-NonCommercial-NoDerivatives 4.0 International

# Proximity of 2D boxes within any given area
# You hand in the Array of many [[Location Details], [Object(s)]]
# And you will get back [ [ [Location Details], [Object(s)],
# [nil 'aka self', Relational Strength To 1, etc] ],
# [ [Location Details], [Object(s)],
# [Relational Strength To 0, nil 'aka self', etc] ] ]

# Rules
# box = Array [:x, :y, :width, :height]
# pbox = Array [box, Array Object.new]

#  Points are indexed as follows in results
#
#   1------------2
#   |            |
#   |            |
#   3------------4
#
#  Expect two_demmension_compare to give strengths back as
#  {
#     :horizontal => [1<->2, 2<->1, 3<->4, 4<->3],
#     :vertical   => [1<->3, 2<->4, 3<->1, 4<->2]
#  }


class ProxBox
  attr_accessor :pbox

  def initialize( params = {} )
    @pbox = params.fetch(:pbox, nil)
    if !!@pbox
      raise "Invalid list of layout details!" unless @pbox.all? { |grid, obj|
        grid.all? { |layout|
          layout.is_a?(Fixnum)
        } && grid.length == 4
      }
      @pbox = pop_relationships(@pbox)
    end
  end

  def pop_relationships(everything)
    raise "Incorrect Array length(s)!  Each must be 2 in length!" unless everything.all? {|i| i.length == 2}
    everything.each_with_index { |item, index|
      everything.each_with_index { |sectem, secdex|
        unless index == secdex
          everything[index][2] = Array(everything[index][2]) << overall_relationship(item[0],sectem[0])
        else
          everything[index][2] = Array(everything[index][2]) << nil
        end
      }
    }
    everything
  end

  def euclidean_distance(a,b)
    raise "Array's required!" unless a.is_a?(Array) && b.is_a?(Array)
    raise "Array's must contain Integers!" unless [a,b].all? {|h| h.all? {|i| i.is_a?(Fixnum)} }
    Math.sqrt(a.zip(b).map { |x| (x[1] - x[0])**2 }.reduce(:+))
  end

  def two_dimension_compare(a,b)
    tdep = two_dimension_edge_points(a,b)
    {
      # :horizontal => [1<->2, 2<->1, 3<->4, 4<->3]
      :horizontal => [
        euclidean_distance( *tdep[:horizontal][0] ),
        euclidean_distance( *tdep[:horizontal][1] ),
        euclidean_distance( *tdep[:horizontal][2] ),
        euclidean_distance( *tdep[:horizontal][3] )
      ],

      # :vertical   => [1<->3, 2<->4, 3<->1, 4<->2]
      :vertical => [
        euclidean_distance( *tdep[:vertical][0] ),
        euclidean_distance( *tdep[:vertical][1] ),
        euclidean_distance( *tdep[:vertical][2] ),
        euclidean_distance( *tdep[:vertical][3] )
      ]
    }
  end

  def two_dimension_edge_points(a,b)
    a = Array four_points(*a)
    b = Array four_points(*b)
    {
      # :horizontal => [1<->2, 2<->1, 3<->4, 4<->3]
      :horizontal => [
        [ a[0..1], [b[2],b[1]] ],
        [ [a[2],a[1]], b[0..1] ],
        [ [a[0],a[3]], b[2..3] ],
        [ a[2..3], [b[0],b[3]] ]
      ],

      # :vertical   => [1<->3, 2<->4, 3<->1, 4<->2]
      :vertical => [
        [ a[0..1], [b[0],b[3]] ],
        [ [a[2],a[1]], b[2..3] ],
        [ [a[0],a[3]], b[0..1] ],
        [ a[2..3], [b[2],b[1]] ]
      ]
    }
  end

  def strongest_relationship(arr)
    closest_by_value(arr)
  end

  def four_points(x,y,width,height)
    Array( [x,y,x+width,y+height] )
  end

  def closest_by_value(array_points, value = 0)
    array_points.min_by { |x| (x.to_f - value).abs }
  end

  def closest_by_index(array_points, value = 0)
    array_points.index (
      closest_by_value(array_points, value)
    )
  end

  def overall_relationship(a,b)
    hash = two_dimension_compare(a,b)
    closest_by_value(
      [  closest_by_value( hash[ :vertical   ] ),
         closest_by_value( hash[ :horizontal ] )  ]
    )
  end
end
