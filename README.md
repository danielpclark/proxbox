
```
#!ruby

# Title::     Proximity Box (proxbox)
# Author::    Daniel P. Clark  (mailto:6ftdan@gmail.com)
# License::   Attribution-NonCommercial-NoDerivatives 4.0 International

# Proximity of 2D boxes within any given area
# You hand in the Array of many [[Location Details], [Object(s)]]
# And you will get back [ [ [Location Details], [Object(s)],
# [nil 'aka self', Relational Strength To 1, etc] ],
# [ [Location Details], [Object(s)],
# [Relational Strength To 0, nil 'aka self', etc] ] ]

# Rules
# box = Array [:x, :y, :width, :height]
# pbox = Array [box, Array Object.new]

#  Points are indexed as follows in results
#
#   1------------2
#   |            |
#   |            |
#   3------------4
#
#  Expect two_demmension_compare to give strengths back as
#  {
#     :horizontal => [1<->2, 2<->1, 3<->4, 4<->3],
#     :vertical   => [1<->3, 2<->4, 3<->1, 4<->2]
#  }
```
